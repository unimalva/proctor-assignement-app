# CI Pipeline for nginx container

Repo that contains the docker image for nginx  + CI pipeline.

## CI build

The CI pipeline builds an image and push to a aws ECR registry.
The  branch model followed is the gitlab flow, having a master branch for building and pushing development images and a subsequent release branch for building releases, using as tag strategy the commit short SHA1, instead of a semantic tagging, this way is easier to match a given image to its commit id.

### Merge strategy

The Push Development job will run on commit to master branch.
The Push Release job will run on commit to release branch.


### Pre-requisite

In order to deploy to AWS it is required to setup a iam user with the following policy statement attached.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:*"
            ],
            "Resource": "arn:aws:ecr:<region>:<accountid>:repository/nginx"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken"
            ],
            "Resource": "*"
        }
    ]
}
```

### Instructions

1. . Create a new repo on gitlab + create branch master and release, configure them as protected branches.
2. On a AWS account :
    . ##Create a s3 bucket and a ddb table to manage tf state.
    . ##Add permissions for 
    . Create an AWS IAM with only programatic access, download the csv file with the access key and secret key on creation of the user.
    . Create the policy in pre-requisits.
    . Attach the policy to the user created previously
    . Create a AWS ECR registry, the registry used was ```nginx```, if another name used, please change accordingly.
5. Configure on the repo CI/CD  as procted and masked variables:  ```AWS_ACCESS_KEY```; and ```AWS_SECRET_KEY``` from the user created in pont 2.
6. Add a third protected variable ```ECR_REPO``` with the URI from the repo created before.
 
 

#### Issues

Known issue : ```WARNING! Using --password via the CLI is insecure. Use --password-stdin.```

There is a warning message during docker push to the ECR registry, it's a known issue. A solution exist, having the command used for docker push implementing ```--password-stdin``` prevents the password from ending up in the shell's history, or log-files.
A solution needs to be implemented around the parameter described above.




